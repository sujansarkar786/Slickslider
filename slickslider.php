<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             1.0.0
 * @package           Slickslider
 *
 * @wordpress-plugin
 * Plugin Name:       slick slider
 * Plugin URI:        #
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Sujan Miya
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       slickslider
 * Domain Path:       /languages
 */

class SlickSlider
{
    public function __construct()
    {
        add_action('plugins_loaded', array(
            $this,
            'loadtext_domain'
        ));
        add_action('wp_enqueue_scripts', array(
            $this,
            'load_slider_slik'
        ));
        add_action('wp_enqueue_scripts', array(
            $this,
            'slickMaincss'
        ));
        add_action('wp_enqueue_scripts', array(
            $this,
            'slickMainjs'
        ));
        add_action('wp_enqueue_scripts', array(
            $this,
            'customJs'
        ));
        add_action('init', array(
            $this,
            'slicksliderCustomField'
        ));
        add_shortcode('slick-slider-custom', array(
            $this,
            'slicksliderShortcode'
        ));
        add_filter('plugin_action_links_' . plugin_basename(__FILE__) , array(
            $this,
            'sliderLinkSetting'
        ));

    }

    public function loadtext_domain()
    {
        load_plugin_textdomain('slick-slider', false, plugin_dir_url(__FILE__) . "/languages");
    }
    public function load_slider_slik()
    {
        wp_enqueue_style('slickslider-theme', plugin_dir_url(__FILE__) . "assets/css/slick-theme.css");
    }
    public function slickMaincss()
    {
        wp_enqueue_style('slick-slider-main', plugin_dir_url(__FILE__) . "assets/css/slick.css");
    }
    public function slickMainjs()
    {
        wp_enqueue_script('main-slick', plugin_dir_url(__FILE__) . "assets/js/slick.min.js", array(
            'jquery'
        ) , true);
    }
    public function customJs()
    {
        wp_enqueue_script('custom-slick-js', plugin_dir_url(__FILE__) . "assets/js/custom.js", array(
            'jquery'
        ) , true);
        $slidesToShow = get_option('slick_select_field_1') ? get_option('slick_select_field_1'): 2;

        wp_localize_script('custom-slick-js', 'slidesToShow', $slidesToShow);
        $slidesToShow1 = get_option('slick_select_field_2') ? get_option('slick_select_field_2'): 2;

        wp_localize_script('custom-slick-js', 'slidesToShow1', $slidesToShow1);
    }
    public function sliderLinkSetting($links)
    {
        $newLinks = sprintf("<a href='%s'>%s</a>", 'options-general.php?page=slick_slider_', __('Setting', 'slickslider'));
        $links[] = $newLinks;
        return $links;

    }
    public function slicksliderCustomField()
    {
        register_post_type('slick-slider-custom', array(
            'labels' => array(
                'name' => 'Slick Slider',
                'add_new_item' => 'Add Slider',
                'add_new' => 'Add New Slider',
                'all_items' => 'All Slider'
            ) ,
            'public' => true,
            'supports' => array(
                'thumbnail'
            )
        ));
    }
    public function slicksliderShortcode()
    {
        ob_start();

?>
   <?php
        $slickSlider = new WP_Query(array(
            'post_type' => 'slick-slider-custom',
            'posts_per_page' => - 1
        ));
?>
   <?php $options = get_option('slick_settings'); ?>
   <?php if (have_posts() && $options['slick_select_field_0'] == 1): ?> 
  <section class="vertical-center-4 slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
  <?php if (have_posts() && $options['slick_select_field_0'] == 2): ?> 
  <section class="vertical-center-3 slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
  <!-- style 3 -->
  <?php if (have_posts() && $options['slick_select_field_0'] == 3): ?> 
  <section class="vertical-center-2 slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
   <!-- style 4 -->
   <?php if (have_posts() && $options['slick_select_field_0'] == 4): ?> 
  <section class="vertical-center slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
   <!-- style 5 -->
   <?php if (have_posts() && $options['slick_select_field_0'] == 5): ?> 
  <section class="vertical slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
    <!-- style 6 -->
    <?php if (have_posts() && $options['slick_select_field_0'] == 6): ?> 
  <section class="regular slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
      <!-- style 7 -->
      <?php if (have_posts() && $options['slick_select_field_0'] == 7): ?> 
  <section class="center slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
      <!-- style 8 -->
      <?php if (have_posts() && $options['slick_select_field_0'] == 8): ?> 
  <section class="variable slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
     <!-- style 9 -->
     <?php if (have_posts() && $options['slick_select_field_0'] == 9): ?> 
  <section class="lazy slider">
 
   <?php while ($slickSlider->have_posts()):
                $slickSlider->the_post(); ?>
    <div>
      <?php the_post_thumbnail(); ?>
    </div>
    <?php
            endwhile; ?>

  </section>
  <?php
        endif; ?>
    <?php
        return ob_get_clean();
    }

}
new SlickSlider();

add_action('admin_menu', 'slick_add_admin_menu');
add_action('admin_init', 'slick_settings_init');

function slick_add_admin_menu()
{

    add_options_page('slick slider ', 'slick slider ', 'manage_options', 'slick_slider_', 'slick_options_page');

}

function slick_settings_init()
{

    register_setting('pluginPage', 'slick_settings');
    register_setting( 'pluginPage', 'slick_select_field_1', array( 'sanitize_callback' => 'esc_attr' ) );
    register_setting( 'pluginPage', 'slick_select_field_2', array( 'sanitize_callback' => 'esc_attr' ) );

    add_settings_section('slick_pluginPage_section', __('Your section description', 'slickslider') , 'slick_settings_section_callback', 'pluginPage');

    add_settings_field('slick_select_field_0', __('Settings field description', 'slickslider') , 'slick_select_field_0_render', 'pluginPage', 'slick_pluginPage_section');
    add_settings_field('slick_select_field_1', __('Settings field description', 'slickslider') , 'slick_select_field_0_render_text', 'pluginPage', 'slick_pluginPage_section');
    add_settings_field('slick_select_field_2', __('Settings field description', 'slickslider') , 'slick_select_field_0_render_two', 'pluginPage', 'slick_pluginPage_section');
    

}

function slick_select_field_0_render()
{

    $options = get_option('slick_settings');
?>
	<select name='slick_settings[slick_select_field_0]'>
		<option value='1' <?php selected($options['slick_select_field_0'], 1); ?>>Style 1</option>
    <option value='2' <?php selected($options['slick_select_field_0'], 2); ?>>Style 2</option>
    <option value='3' <?php selected($options['slick_select_field_0'], 3); ?>>Style 3</option>
    <option value='4' <?php selected($options['slick_select_field_0'], 4); ?>>Style 4</option>
    <option value='5' <?php selected($options['slick_select_field_0'], 5); ?>>Style 5</option>
    <option value='6' <?php selected($options['slick_select_field_0'], 6); ?>>Style 6</option>
    <option value='7' <?php selected($options['slick_select_field_0'], 7); ?>>Style 7</option>
    <option value='8' <?php selected($options['slick_select_field_0'], 8); ?>>Style 8</option>
    <option value='9' <?php selected($options['slick_select_field_0'], 9); ?>>Style 9</option>
	</select>

<?php
}
function slick_select_field_0_render_text(){
  $options = get_option('slick_select_field_2');
  echo "<input type='text' name='slick_select_field_2' id='slick_select_field_2' value='".$options."'/>";
}
function slick_select_field_0_render_two(){
    $options = get_option('slick_select_field_1');
    echo "<input type='text' name='slick_select_field_1' id='slick_select_field_1' value='".$options."'/>";
}
function slick_settings_section_callback()
{

    echo __('This section description', 'slickslider');

}

function slick_options_page()
{

?>
		<form action='options.php' method='post'>

			<h2>slick slider </h2>

			<?php
    settings_fields('pluginPage');
    do_settings_sections('pluginPage');
    submit_button();
?>

		</form>
		<?php
}

